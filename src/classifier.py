import os
import kakaovision
from pathlib import Path
from PyQt5.QtCore import pyqtSignal, pyqtSlot, QObject, QCoreApplication


class Classifier(QObject):
    finished = pyqtSignal()

    def __init__(self):
        QObject.__init__(self)
        self.bStop = False
        self.path = ""
        self.api = ""

    def InitParams(self, path, api):
        self.path = path
        self.api = api

    def stop_classify(self):
        self.bStop = True

    @pyqtSlot()
    def classify(self):

        BASE_DIR = Path(self.path)
        EXTENSIONS = {".png", ".jpg"}

        counts = len([name for name in os.listdir(self.path) if os.path.isfile(os.path.join(self.path, name))])

        normalDir = os.path.isdir(self.path + "/normal")
        softDir = os.path.isdir(self.path + "/soft")
        adultDir = os.path.isdir(self.path + "/adult")
        if normalDir is not True:
            os.makedirs(self.path + "/normal")
        if softDir is not True:
            os.makedirs(self.path + "/soft")
        if adultDir is not True:
            os.makedirs(self.path + "/adult")
        print("총 " + str(int(counts)) + "개의 파일 발견")
        img_cnt = 0
        for img in BASE_DIR.glob(r"**/*"):
            if img_cnt == int(counts):
                break;
            if img.suffix in EXTENSIONS :
                print(img.name + " 작업 중....")
                if self.bStop is True:
                    break
                ret, apiError = kakaovision.requst_by_file(self.api, img)

                if apiError is True:
                    print("API 호출 오류로 종료.")
                    self.bStop = True
                    break

                if apiError is False and len(ret) > 0:
                    max_key = max(ret, key = ret.get)
                    if max_key == "normal":
                        print(img.name + " 정상 이미지 입니다.")
                        Path(str(img)).rename(str(BASE_DIR) + "/normal/" + str(img.name))
                    elif max_key == "soft":
                        print(img.name + " 소프트한 이미지 입니다.")
                        Path(str(img)).rename(str(BASE_DIR) + "/soft/" + str(img.name))
                    elif max_key == "adult":
                        print(img.name + " 성인 이미지 입니다.")
                        Path(str(img)).rename(str(BASE_DIR) + "/adult/" + str(img.name))
                else:
                    print(img.name + " 오류")
            else:
                print(img.name + " 포맷 오류")
            QCoreApplication.processEvents()
            img_cnt = img_cnt + 1
        print("분류 종료.")
        self.finished.emit()
