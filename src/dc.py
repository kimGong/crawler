import requests
from bs4 import BeautifulSoup
import time
from datetime import datetime
from urllib import request
# from selenium import webdriver
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.support import expected_conditions as EC
# from selenium.webdriver.common.by import By
import json
import os
from PyQt5.QtCore import pyqtSignal, pyqtSlot, QObject, QCoreApplication
import imghdr
from pathlib import Path
import kakaovision

BASE_URL = "https://gall.dcinside.com"

MINOR_GALL = "https://gall.dcinside.com/mgallery/board/lists"  # 마이너 갤러리 베이스 주소

MAJOR_GALL = "https://gall.dcinside.com/board/lists"  # 메이저 갤러리 베이스 주소

agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36"

headers = {}
headers["User-Agent"] = agent

imgCnt = 1

# 버퍼의 크기
nBuf = 25

startPage = 1

endPage = 10

# 버퍼 카운터
bufCnt = 0

# 0으로 초기화
gallNumBuf = [0] * nBuf

maxAttempt = 10

timestamp = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')

savePath = ""

path = ""


# # 제목 + 내용
# params = {"id": "aoegame", "page": page, "search_pos": "", "s_type": "search_subject_memo", "s_keyword": "꼴"}
# # 제목
# params = {"id": "aoegame", "page": page, "s_type":"search_subject", "s_keyword": "꼴"}
# # 내용
# params = {"id": "aoegame", "page": page, "s_type": "search_memo", "s_keyword": "꼴"}
# # 글쓴이
# params = {"id": "aoegame", "page": page, "s_type": "search_name", "s_keyword": "ㅇㅇ"}
#
# exist = os.path.isdir("../images/" + "comic_new2" + "/" + timestamp) # 다운로드 경로
# if exist == False:
#     path = "../images/" + "comic_new2" + "/" + timestamp
#     os.makedirs(path)
# for page in range(startPage, endPage + 1):
#     # params = {"id": "superidea", "page": page, "search_pos": "", "s_type": "search_subject_memo", "s_keyword": "꼴"}
#     params = {"id": "comic_new2", "page": page}
#     # id는 게시판 이름 page 수는
#     # 지정해서 받음 됨 지금 1~10
#     # 페이지로 설정
#     resp = requests.get(MAJOR_GALL, params = params, headers = headers) # 크롤링 갤러리가 마이너인지 메이너인지 따라 파리미터 전달
#     html = resp.text
#     bigSoup = BeautifulSoup(html, "html.parser")
#     posts = bigSoup.find_all("tr", {"class": "ub-content us-post"})
#     # 경로 생성
#     gallName = params["id"]
#     for p in posts:
#         link = p.a.get("href")
#         if (p.select(".icon_img.icon_pic") or p.select(".icon_img.icon_toprecomimg")
#                  or p.select(".icon_img.icon_recomimg")):
#             gallNum = p.find("td", {"class": "gall_num"})
#
#             if int(gallNum.text) in gallNumBuf:
#                 print("It was already stored")
#                 continue
#             gallNumBuf[bufCnt % nBuf] = int(gallNum.text)
#             bufCnt += 1
#             print(p.text)
#             # print(BASE_URL + link) # 내부 글 링크(이미지가 들어있는 게시글)
#             postLink = BASE_URL + link
#             singlePost = requests.get(postLink, headers = headers)
#             # print(singlePost) # 리스폰스 테스트
#             smallSoup = BeautifulSoup(singlePost.content, "html.parser")
#             dl_links = smallSoup.find("div", class_ = "appending_file_box").find("ul").find_all("li")
#             for dl_link in dl_links:
#                 dl_tag = dl_link.find("a", href = True)
#                 dl_url = dl_tag["href"]
#                 file_ext = dl_url.split(".")[-1]
#                 savename = str(imgCnt) + "." + file_ext
#                 opener = request.build_opener()
#                 opener.addheaders = [("User-agent", agent), ("Referer", singlePost.url)]
#                 request.install_opener(opener)
#                 request.urlretrieve(dl_url, path + "/" + savename)
#                 imgCnt = imgCnt + 1
#                 time.sleep(1.0)
# print("Crawl ends")



class Crawler(QObject):
    finished = pyqtSignal()
    def __init__(self):
        QObject.__init__(self)
        self.path = ""
        self.timeStamp = ""
        self.maxAttempt = 10
        self.startPage = 0
        self.endPage = 0
        self.nBuf = 0
        self.bufCnt = 0
        self.gallNumBuf = 0
        self.imgCnt = 1
        self.completeCnt = 0
        self.url = ""
        self.keyword = ""
        self.keyword_setting = ""
        self.ID = ""
        self.isMinor = False
        self.bStop = False

    def InitParams(self, path, startPage, endPage, nBuf, url, isMinor, id, keyword, keyword_setting):
        self.path = path
        self.startPage = startPage
        self.endPage = endPage
        self.nBuf = nBuf
        self.url = url
        self.isMinor = isMinor
        self.keyword = keyword
        self.keyword_setting = keyword_setting
        self.gallNumBuf = [0] * nBuf
        self.ID = id
        self.timeStamp = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')

    def stop_crawl(self):
        self.bStop = True

    @pyqtSlot()
    def crawl(self):
        path = str(self.path).rstrip()
        stamp = str(self.timeStamp).rstrip()
        params = {}
        params["id"] = self.ID
        if self.keyword_setting == "제목 + 내용":
            params = {"id": self.ID, "search_pos": "", "s_type": "search_subject_memo", "s_keyword": self.keyword}
        elif self.keyword_setting == "제목":
            params = {"id": self.ID, "s_type": "search_subject", "s_keyword": self.keyword}
        elif self.keyword_setting == "내용":
            params = {"id": self.ID, "s_type": "search_memo", "s_keyword": self.keyword}
        elif self.keyword_setting == "글쓴이":
            params = {"id": self.ID, "s_type": "search_name", "s_keyword": self.keyword}

        gallName = params["id"]
        if path == "":
            exist = os.path.isdir("./" + str(gallName) + "/" + stamp)
        else:
            exist = os.path.isdir(path + "/" + str(gallName) + "/" + stamp)

        if exist is False:
            if path == "":
                path = "./" + str(gallName) + "/" + stamp
                os.makedirs(path)
            else:
                path = path + "/" + str(gallName) + "/" + stamp
                os.makedirs(path)

        for page in range(self.startPage, self.endPage + 1):
            print("////////////  " + str(page) + " / " + str(self.endPage) + " ////////////")
            if self.bStop is True:
                break
            params["page"] = page
            if self.isMinor is True:
                resp = requests.get(MINOR_GALL, params = params, headers = headers)
            else:
                resp = requests.get(MAJOR_GALL, params = params, headers = headers)
            html = resp.text
            bigSoup = BeautifulSoup(html, "html.parser")
            posts = bigSoup.find_all("tr", {"class": "ub-content us-post"})
            for p in posts:
                if self.bStop is True:
                    break
                link = p.a.get("href")
                if (p.select(".icon_img.icon_pic") or p.select(".icon_img.icon_toprecomimg")
                 or p.select(".icon_img.icon_recomimg") or p.select(".icon_img.icon_btimebest")):
                    gallNum = p.find("td", {"class": "gall_num"})
                    if int(gallNum.text) in self.gallNumBuf:
                        print("이미 저장된 글")
                        continue
                    self.gallNumBuf[self.bufCnt % self.nBuf] = int(gallNum.text)
                    self.bufCnt += 1
                    postLink = BASE_URL + link
                    singlePost = requests.get(postLink, headers = headers)
                    for attempt in range(10):  # 나중에 변수로 최대 재시도 횟수 설정
                        if self.bStop is True:
                            break
                        try:
                            smallSoup = BeautifulSoup(singlePost.content, "html.parser")
                            dl_links = smallSoup.find("div", class_ = "appending_file_box").find("ul").find_all("li")
                            QCoreApplication.processEvents()
                        except AttributeError as e:
                            print(e)
                            QCoreApplication.processEvents()
                            time.sleep(0.5)
                            continue
                        else:
                            break
                    title = p.find("a")
                    print(title.text + " / 이미지 크롤링 시작.")
                    for dl_link in dl_links:
                        if self.bStop is True:
                            break
                        dl_tag = dl_link.find("a", href = True)
                        dl_url = dl_tag["href"]
                        file_ext = dl_url.split(".")[-1]
                        savename = self.ID + "_" + self.timeStamp + "_" + str(self.imgCnt) + "." + file_ext
                        opener = request.build_opener()
                        opener.addheaders = [("User-agent", agent), ("Referer", singlePost.url)]
                        request.install_opener(opener)
                        request.urlretrieve(dl_url, path + "/" + savename)
                        self.imgCnt += 1
                        QCoreApplication.processEvents()
                        time.sleep(0.3)
            QCoreApplication.processEvents()
        print("크롤링 종료.")
        self.finished.emit()

def GetMGallList():
    dict = {}

    # 파일이 존재하면 파일에서 로드
    if os.path.isfile("majorGalls.json"):
        with open("majorGalls.json") as file:
            dict = json.load(file)
    # 아닐 경우 다시 크롤링 해온다.
    else:
        majorGall = "https://gall.dcinside.com/"
        for i in range(10):
            try:
                resp = requests.get(majorGall, headers = headers)
                print(resp.status_code)
            except requests.exceptions.RequestException:
                time.sleep(0.5)
                continue
            else:
                html = resp.text
                soup = BeautifulSoup(html, "html.parser")
                cate = soup.find("div", id = "categ_listwrap", class_ = "cate_wrap")
                uls = cate.find_all("ul")
                for ul in uls:
                    lis = ul.find_all("li")
                    for li in lis:
                        find = li.find("a")
                        if find is not None:
                            a = li.a.get("href")
                            dict[li.text.rstrip()] = a

                cate = soup.find("div", class_ = "section_cate new_gallery")
                ul = cate.find("ul")
                lis = ul.find_all("li")
                for li in lis:
                    find = li.find("a")
                    if find is not None:
                        a = li.a.get("href")
                        dict[li.text.rstrip()]= a
                break
        # 반환하기전에 저장
        with open("majorGalls.json", "w") as file:
            file.write(json.dumps(dict))

    return dict


def GetGallName(url):
    name = ""
    for i in range(10):
        try:
            resp = requests.get(url, headers = headers)
            print(resp.status_code)
        except requests.exceptions.RequestException:
            time.sleep(0.1)
            continue
        else:
            html = resp.text
            soup = BeautifulSoup(html, "html.parser")
            head = soup.find("div", class_ = "page_head clear")
            a = head.find("a")
            name = a.text.rstrip()
            break
    return name


# 속도 문제로 안 쓰는 코드
# def GetmGallList():
#     dict = {}
#
#     if os.path.isfile("minorGalls.json"):
#         with open("minorGalls.json") as file:
#             dict = json.load(file)
#     else:
#         minorGall = "https://gall.dcinside.com/m"
#         options = webdriver.ChromeOptions()
#         options.headless = True
#         options.add_argument("user-agent = Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, "
#                              "like Gecko) "
#                              "Chrome/87.0.4280.141 Safari/537.36")
#         browser = webdriver.Chrome(executable_path = "../driver/chromedriver.exe", options = options)
#         browser.get(minorGall)
#         buttons = browser.find_elements_by_xpath("//button[@class = 'btn_cate_more']")
#         WebDriverWait(browser, timeout = 5)
#         for b in buttons:
#             webdriver.ActionChains(browser).move_to_element(b).click(b).perform()
#             # 버튼을 눌러서 카테고리가 열렀으니 이제 여기서 목록들을 가져온다.
#             WebDriverWait(browser, timeout = 5).until(EC.presence_of_element_located
#                                                       ((By.CSS_SELECTOR, "div.pop_content.under_catelist.fiveline")))
#             popUpHtml = browser.page_source
#             soup = BeautifulSoup(popUpHtml, "html.parser")
#             popUp = soup.find("div", class_ = "pop_content under_catelist fiveline")
#             Atags = popUp.find_all("a", class_ = "list_title")
#             for a in Atags:
#                 span = a.find("span", class_ = "name")
#                 link = a.get("href")
#                 dict[span.text.rstrip()] = link
#         browser.quit()
#         for i in range(10):
#             try:
#                 resp = requests.get(minorGall, headers = headers)
#                 print(resp.status_code)
#             except requests.exceptions.RequestException:
#                 time.sleep(0.5)
#                 continue
#             else:
#                 html = resp.text
#                 soup = BeautifulSoup(html, "html.parser")
#                 cate = soup.find("div", id = "categ_listwrap", class_ = "cate_wrap")
#                 uls = cate.find_all("ul")
#                 for ul in uls:
#                     lis = ul.find_all("li")
#                     for li in lis:
#                         find = li.find("a")
#                         if find is not None:
#                             a = li.a.get("href")
#                             dict[li.text.rstrip()] = a
#                 break
#
#     with open("minorGalls.json", "w") as file:
#         file.write(json.dumps(dict))
#     return dict
