import sys
import dc
# import classifier
import icons
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QLineEdit, QVBoxLayout, QHBoxLayout, \
    QPushButton, QScrollArea, QGroupBox, QFormLayout, QComboBox, QFileDialog, QProgressBar, QTabWidget, QSpinBox, \
    QTextEdit
from PyQt5.QtCore import Qt, QObject, pyqtSignal, QThread
from PyQt5.QtGui import QIcon, QTextCursor, QFont


class MyApp(QWidget):

    def __init__(self):
        super().__init__()
        self.initUI()
        # self.mGallDict
        self.targetURL = ""
        self.targetID = ""

    def initUI(self):
        # 크롤링 타겟 갤러리 보여주는 박스
        infoGroupBox = QGroupBox("갤러리 정보")
        self.gallName = QLabel("타겟 갤러리 이름 : ")
        self.url = QLabel("타겟 갤러리 URL : ")
        self.ID = QLabel("갤러리 ID : ")
        infoBox = QVBoxLayout()

        # 왼쪽의 갤러리 리스트과 폼 레이아웃 그리고 검색창
        # 메이저 갤러리 설정과 버튼 리스트
        MGallGroup = QGroupBox("메이저 갤러리 리스트")
        self.majorForm = QFormLayout()
        self.MGallSearch = QLineEdit()
        self.MGallSearch.setToolTip("메이저 갤러리를 검색합니다.")
        self.MGallSearch.textChanged[str].connect(self.onChanged)
        self.MGallDict = dc.GetMGallList()
        for key, value in self.MGallDict.items():
            temp = QPushButton(key)
            temp.clicked.connect(self.setMajorgall)
            self.majorForm.addRow(temp)

        # 마이너 갤러리
        # 원래는 버튼으로 전부 다 저장했으나 너무 느려서 URL 설정하는 식으로 변경
        mGallGroup = QGroupBox("마이너 갤러리 설정")
        minorForm = QFormLayout()
        self.mGallSearch = QLineEdit()
        mGallSetBtn = QPushButton("설정")
        mGallSetBtn.setToolTip("해당 갤러리를 크롤링 타겟으로 설정합니다.")
        mGallSetBtn.clicked.connect(self.setmGall)
        minorForm.addRow("갤러리 주소", self.mGallSearch)
        minorForm.addRow(mGallSetBtn)
        # self.mGallDict = dc.GetmGallList()
        # for key, value in self.mGallDict.items():
        # minorForm.addRow(QPushButton(key))

        # 갤러리 정보 상자
        infoBox.addWidget(self.gallName)
        infoBox.addWidget(self.url)
        infoBox.addWidget(self.ID)
        infoBox.setContentsMargins(0, 0, 0, 0)
        infoGroupBox.setLayout(infoBox)

        # 오른쪽의 검색 설정과 검색창
        self.searchKeyword = QLineEdit()
        self.searchKeyword.setToolTip("검색 키워드를 설정합니다. (공백일시 페이지의 모든 그림이 있는 글을 크롤링합니다.)")
        self.keywordCB = QComboBox()

        self.keywordCB.addItem("제목 + 내용")
        self.keywordCB.addItem("제목")
        self.keywordCB.addItem("내용")
        self.keywordCB.addItem("글쓴이")

        # 경로 버튼과 시그널 설정
        dirBtn = QPushButton()
        dirBtn.setIcon(QIcon(":/icons/path.png"))
        dirBtn.setToolTip("저장 경로를 설정합니다. (공백일시 해당 프로그램 경로에 저장)")
        self.DIRPATH = QLineEdit()
        self.DIRPATH.setReadOnly(True)
        dirBtn.clicked.connect(self.dirBtn_hanlder)

        # 크롤링 버튼과 팝업 창 설정과 시그널 설청
        startBtn = QPushButton("크롤링 시작")
        self.crawl = PopUpCrawl()
        startBtn.clicked.connect(self.crawl_start)

        # 왼쪽의 레이아웃 설정과 스크롤 가능 여부를 설정
        MGallGroup.setLayout(self.majorForm)
        mGallGroup.setLayout(minorForm)

        self.majorScrollArea = QScrollArea()
        self.majorScrollArea.setWidget(MGallGroup)
        self.majorScrollArea.setWidgetResizable(True)
        self.majorScrollArea.setFixedHeight(400)
        self.majorScrollArea.setFixedWidth(400)

        # 전체적인 레이아웃은 수평 박스 레이아웃으로
        hbox = QHBoxLayout()

        # 검색 요소를 위한 수평 박스 레이아웃
        searchHBox = QHBoxLayout()

        # 검색 설정 박스 레이아웃
        searchHBox.addWidget(self.keywordCB)
        searchHBox.addWidget(self.searchKeyword)

        # 저장 경로를 위한 수평 박스 레이아웃
        dirHBox = QHBoxLayout()
        dirHBox.addWidget(dirBtn)
        dirHBox.addWidget(self.DIRPATH)

        # 코를링 시작 페이지와 끝 페이지 설정
        pageHBox = QHBoxLayout()
        self.StartPage = QSpinBox()
        self.StartPage.setToolTip("크롤링 시작 페이지를 설정합니다.")
        self.EndPage = QSpinBox()
        self.EndPage.setToolTip("크롤링 마지막 페이지를 설정합니다.")
        self.StartPage.setRange(1, 2147483647)
        self.EndPage.setRange(1, 2147483647)
        self.StartPage.setSingleStep(1)
        self.EndPage.setSingleStep(1)
        self.StartPage.valueChanged.connect(self.nPageChanged)
        self.EndPage.valueChanged.connect(self.nPageChanged)
        pageHBox.addWidget(QLabel("시작 페이지"))
        pageHBox.addWidget(self.StartPage)
        pageHBox.addWidget(QLabel("마지막 페이지"))
        pageHBox.addWidget(self.EndPage)

        # 크롤링시 버퍼의 크기
        bufferHBox = QHBoxLayout()
        bufferLabel = QLabel("버퍼 사이즈")
        bufferLabel.setAlignment(Qt.AlignLeft)
        self.BufferSize = QSpinBox()
        self.BufferSize.setToolTip("버퍼의 크기를 지정합니다. (글 리젠이 많은 갤러리를 크롤링시 크기를 키우는 것을 권장)")
        self.BufferSize.setMinimum(10)
        self.BufferSize.setMaximum(50)
        self.BufferSize.setSingleStep(1)
        self.BufferSize.setAlignment(Qt.AlignLeft)
        bufferHBox.addWidget(bufferLabel)
        bufferHBox.addWidget(self.BufferSize)

        # KAKAO API KEY를 위한 박스 레이아웃
        # self.classifier = PopUpClassifier()
        # apiVBox = QVBoxLayout()
        # apiHBox = QHBoxLayout()
        # apiPathBox = QHBoxLayout()
        # API_dirBtn = QPushButton()
        # classifyBtn = QPushButton("분류")
        # classifyBtn.clicked.connect(self.classify_start)
        # API_dirBtn.setIcon(QIcon(":/icons/Classification.png"))
        # API_dirBtn.clicked.connect(self.open_api_dialog)

        # api = QLabel("카카오 API")
        # self.APIKEY = QLineEdit()
        # self.APIPATH = QLineEdit()
        # apiPathBox.addWidget(API_dirBtn)
        # apiPathBox.addWidget(self.APIPATH)
        # apiPathBox.addWidget(classifyBtn)
        # apiVBox.addLayout(apiHBox)
        # apiVBox.addLayout(apiPathBox)

        # # 카카오 API 분류를 위한 박스 레이아웃
        # self.APIKEY.setToolTip("카카오 비전 REST API를 이용해서 필터링.")
        # apiHBox.addWidget(api)
        # apiHBox.addWidget(self.APIKEY)

        # 갤러리 리스트를 담을 레이아웃(왼쪽)
        # 탭 설정
        self.tabs = QTabWidget()

        # 메이저 갤러리 박스 레이아웃 설정 후 스크롤 영역 추가
        MgallBox = QVBoxLayout()
        majorWidget = QWidget()
        gallSearchHBox = QHBoxLayout()
        gallSearchHBox.addWidget(QLabel("갤러리 검색"))
        gallSearchHBox.addWidget(self.MGallSearch)
        MgallBox.addLayout(gallSearchHBox)
        MgallBox.addWidget(self.majorScrollArea)
        majorWidget.setLayout(MgallBox)

        # 마이너 갤러리 박스 레이아웃 설정 후 스크롤 영역 추가
        mGallBox = QVBoxLayout()
        minorWidget = QWidget()
        mGallBox.addWidget(mGallGroup)
        minorWidget.setLayout(mGallBox)

        # 탭에 2가지 위젯을 추가
        self.tabs.addTab(majorWidget, "메이저 갤러리 목록")
        self.tabs.addTab(minorWidget, "마이너 갤러리")

        # 최종적인 왼쪽 박스 레이아웃을 만들고 탭을 추가한다.
        gallbox = QVBoxLayout()
        gallbox.addWidget(self.tabs)

        # 설정 항목을 담을 레이아웃
        settingBox = QVBoxLayout()
        settingBox.addWidget(infoGroupBox)
        settingBox.addLayout(searchHBox)
        settingBox.addLayout(pageHBox)
        settingBox.addLayout(bufferHBox)
        # API 박스
        # settingBox.addLayout(apiVBox)
        settingBox.addLayout(dirHBox)
        settingBox.addWidget(startBtn)

        # 최종적으로 수평으로 두개의 큰 레이아웃이 순서대로 들어감
        hbox.addLayout(gallbox)
        hbox.addLayout(settingBox)
        self.setLayout(hbox)
        self.setWindowIcon(QIcon(":/icons/dc.png"))
        self.setWindowTitle("DC 크롤러")
        self.setGeometry(200, 300, 1000, 500)
        self.show()

    def __del__(self):
        sys.stdout = sys.__stdout__

    def dirBtn_hanlder(self):
        self.open_dialog_box()

    def API_dirBtn_handler(self):
        self.open_dialog_box()

    def open_api_dialog(self):
        dialog = QFileDialog()
        path = dialog.getExistingDirectory(None, "분류할 곳을 지정해주세요.")
        self.APIPATH.setText(path)

    def open_dialog_box(self):
        dialog = QFileDialog()
        path = dialog.getExistingDirectory(None, "저장 경로를 선택해주세요.")
        self.DIRPATH.setText(path)

    def setMajorgall(self):
        key = self.sender().text()
        url = self.MGallDict[key]
        self.gallName.setText("타겟 갤러리 : " + key)
        self.url.setText("타겟 갤러리 URL : " + url)
        b, k, a = url.partition("?id=")
        self.targetID = a
        self.ID.setText("타겟 갤러리 ID : " + a)
        self.targetURL = self.MGallDict[key]

    def crawl_start(self):
        if self.targetURL == "" or self.targetID == "":
            return
        startPage = self.StartPage.value()
        endPage = self.EndPage.value()
        self.crawl.set_pages(startPage, endPage)
        self.crawl.set_bufsize(self.BufferSize.value())
        self.crawl.set_keyword_setting(self.keywordCB.currentText())
        self.crawl.set_keyword(self.searchKeyword.text())
        self.crawl.set_url(self.targetURL)
        self.crawl.set_path(self.DIRPATH.text())
        self.crawl.set_id(self.targetID)
        self.crawl.show()
        self.crawl.thread_start()
    #
    # def classify_start(self):
    #     if self.APIPATH == "":
    #         return
    #     self.classifier.set_path(self.APIPATH.text())
    #     self.classifier.set_api(self.APIKEY.text())
    #     self.classifier.show()
    #     self.classifier.thread_start()

    def setmGall(self):
        url = self.mGallSearch.text()
        if "https://gall.dcinside.com/mgallery/board" in url:
            if "page" in url:
                b, k, a = url.partition("&page")
                url = b
            name = dc.GetGallName(url)
            self.targetURL = url
            self.gallName.setText("타겟 갤러리 : " + name)
            self.url.setText("타겟 갤러리 URL : " + url)
            b, k, a = url.partition("?id=")
            self.ID.setText("타겟 갤러리 ID : " + a)
            self.targetID = a
            self.mGallSearch.setText("")

    def onChanged(self, text):
        widgets = (self.majorForm.itemAt(i).widget() for i in range(self.majorForm.count()))
        for widget in widgets:
            if isinstance(widget, QPushButton) and text in widget.text():
                self.majorScrollArea.ensureWidgetVisible(widget)
                break

    def nPageChanged(self):
        start = self.StartPage.value()
        end = self.EndPage.value()
        if end < start:
            self.StartPage.setValue(end)
        elif start > end:
            self.EndPage.setValue(start)


class Stream(QObject):
    newText = pyqtSignal(str)

    def write(self, text):
        self.newText.emit(str(text))

    def flush(self):
        pass


# class PopUpClassifier(QWidget):
#     def __init__(self):
#         super().__init__()
#         self.path = ""
#         self.api = ""
#         self.process = QTextEdit(self)
#         self.process.moveCursor(QTextCursor.Start)
#         self.process.ensureCursorVisible()
#         self.process.setLineWrapColumnOrWidth(700)
#         self.process.setLineWrapMode(QTextEdit.FixedPixelWidth)
#         self.process.setFixedWidth(700)
#         self.process.setFixedHeight(500)
#         self.process.setReadOnly(True)
#         self.process.setFont(QFont("Arial", 10, QFont.Bold))
#         self.layout = QVBoxLayout()
#         self.layout.addWidget(self.process)
#         self.setLayout(self.layout)
#         self.setGeometry(300, 300, 550, 100)
#         self.setWindowIcon(QIcon(":/icons/dc.png"))
#         self.setWindowTitle("분류 상황")
#         self.setWindowModality(Qt.ApplicationModal)
#
#     def set_path(self, path):
#         self.path = path
#
#     def set_api(self, api):
#         self.api = api
#
#     def onUpdateConsole(self, text):
#         cursor = self.process.textCursor()
#         cursor.movePosition(QTextCursor.End)
#         cursor.insertText(text)
#         self.process.setTextCursor(cursor)
#         self.process.ensureCursorVisible()
#
#     def thread_start(self):
#         sys.stdout = Stream(newText = self.onUpdateConsole)
#         self.classifier = classifier.Classifier()
#         self.classifier.InitParams(self.path, self.api)
#         self.thread = QThread()
#         self.classifier.moveToThread(self.thread)
#         self.classifier.finished.connect(self.thread.quit)
#         self.thread.started.connect(self.classifier.classify)
#         self.thread.start()
#
#     def closeEvent(self, event):
#         self.process.clear()
#         self.classifier.stop_classify()
#         sys.stdout = sys.__stdout__
#         del self.classifier


class PopUpCrawl(QWidget):
    def __init__(self):
        super().__init__()
        self.startPage = 0
        self.endPage = 0
        self.nBuf = 0
        self.keyword = ""
        self.url = ""
        self.ID = ""
        self.keyword_setting = ""
        self.api = ""
        self.path = ""
        # 팝업창에 터미널을 띄우기 위해서 만듦
        self.process = QTextEdit(self)
        self.process.moveCursor(QTextCursor.Start)
        self.process.ensureCursorVisible()
        self.process.setLineWrapColumnOrWidth(700)
        self.process.setLineWrapMode(QTextEdit.FixedPixelWidth)
        self.process.setFixedWidth(700)
        self.process.setFixedHeight(500)
        self.process.setReadOnly(True)
        self.process.setFont(QFont("Arial", 10, QFont.Bold))
        self.layout = QVBoxLayout()
        self.layout.addWidget(self.process)
        self.setLayout(self.layout)
        self.setGeometry(300, 300, 550, 100)
        self.setWindowIcon(QIcon(":/icons/dc.png"))
        self.setWindowModality(Qt.ApplicationModal)
        self.setWindowTitle("진행 상황")

    def onUpdateConsole(self, text):
        cursor = self.process.textCursor()
        cursor.movePosition(QTextCursor.End)
        cursor.insertText(text)
        self.process.setTextCursor(cursor)
        self.process.ensureCursorVisible()

    def thread_start(self):
        sys.stdout = Stream(newText = self.onUpdateConsole)
        self.crawler = dc.Crawler()
        self.crawler.InitParams(self.path, self.startPage, self.endPage, self.nBuf, self.url, self.isMinor, self.ID, \
                                self.keyword, \
                                self.keyword_setting)
        self.thread = QThread()
        self.crawler.moveToThread(self.thread)
        self.crawler.finished.connect(self.thread.quit)
        self.thread.started.connect(self.crawler.crawl)
        self.thread.start()

    def closeEvent(self, event):
        self.process.clear()
        self.crawler.stop_crawl()
        sys.stdout = sys.__stdout__
        del self.crawler

    def set_pages(self, startPage, endPage):
        self.startPage = startPage
        self.endPage = endPage

    def set_bufsize(self, nBuf):
        self.nBuf = nBuf

    def set_keyword(self, keyword):
        self.keyword = keyword

    def set_url(self, url):
        self.url = url
        self.isMinor = False
        if "mgallery" in url:
            self.isMinor = True

    def set_id(self, id):
        self.ID = id

    def set_keyword_setting(self, setting):
        self.keyword_setting = setting

    def set_path(self, path):
        self.path = path

    def InitCrawler(self):
        self.crawler.InitParams(self.path, self.startPage, self.endPage, self.nBuf, self.url, self.isMinor, self.ID, \
                                self.keyword, \
                                self.keyword_setting)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = MyApp()
    sys.exit(app.exec_())
