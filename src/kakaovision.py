import requests

url = "https://dapi.kakao.com/v2/vision/adult/detect"
# MYAPP_KEY = 'c1a21af1b5613211bb1a17c69cf510f8'
# headers = {'Authorization': 'KakaoAK {}'.format(MYAPP_KEY)}

# URL로 호출
#image_url='https://img1.daumcdn.net/thumb/R720x0.q80/?scode=mtistory&fname=http%3A%2F%2Fcfile8.uf.tistory.com%2Fimage%2F2519554059551FAA2B2B4A'
#data = { 'image_url' : image_url}
#response = requests.post(url, headers=headers, data=data)

# 파일로 호출
# filename = '../images/aoegame/2021-01-17_19-48-02/108_.jpg'
# files = {'image': open(filename, 'rb')}
# response = requests.post(url, headers=headers, files=files)

def requst_by_file(apikey, filepath):
    headers = {"Authorization": "KakaoAK {}".format(apikey)}
    files = {"image": open(filepath, "rb")}
    ret = {}
    try:
        response = requests.post(url, headers = headers, files = files)
        response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        error = response.json()
        code = error["code"]
        if code == "101":
            print("이미지 파일 오류.")
            return ret, False
        elif code == 102:
            print("이미지 포맷 오류.")
            return ret, False
        elif code == 103:
            print("이미지의 크기가 너무 큼.")
            return ret, False
        elif code == 105:
            print("이미지의 용량이 10메가 이하여야 함.")
            return ret, False
        elif code == 404:
            print("API 서버 이미지 다운로드 오류.")
            return ret, False
        print("API 호출 오류.")
        return ret, True

    result = response.json()

    normal = result['result']['normal']
    soft = result['result']['soft']
    adult = result['result']['adult']

    # strnormal = "normal:%f" % (normal)
    # strsoft = "soft:%f" % (soft)
    # stradult = "adult:%f" % (adult)

    ret["normal"] = normal
    ret["soft"] = soft
    ret["adult"] = adult

    return ret, False

    # print(strnormal)
    # print(strsoft)
    # print(stradult)

# # matplot 출력
# import matplotlib.pyplot as plt
# import matplotlib.image as mpimg
# import matplotlib.patches as patches
# # % matplotlib
# # inline
#
# img = mpimg.imread(filename)
# fig, ax = plt.subplots(figsize=(10, 10))
#
# rect = patches.Rectangle((0, 0), 0, 0, lw=5, edgecolor='c', facecolor='none')
# ax.add_patch(rect)
# plt.text(0,5, strnormal, size=15, color='red')
# plt.text(0,25, strsoft, size=15, color='red')
# plt.text(0,45, stradult, size=15, color='red')
#
# ax.imshow(img)
# plt.show()

